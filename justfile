run: update
	ansible-playbook -K --ask-vault-password playbook.yaml

install-nix:
  sh <(curl -L https://nixos.org/nix/install) --daemon < /dev/null

clean:
  nix-collect-garbage

install:
  NIXPKGS_ALLOW_UNFREE=1 nix-env -if ./packages.nix
  just clean

update:
  nix-env -u '*'
  just clean
