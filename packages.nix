{ pkgs ? import <nixpkgs> { } }:

with pkgs; [
  # cli utils
  ansible
  bat
  delta
  duf
  exa
  fd
  gh
  just
  k9s
  lazygit
  lz4
  mosh
  neovim
  nodejs-18_x
  procs
  pv
  ranger
  redis
  ripgrep
  sd
  tldr
  zoxide

  # core functionality
  docker
  docker-compose
  podman
  yarn
  yubico-piv-tool

  # applications
  obsidian
]
