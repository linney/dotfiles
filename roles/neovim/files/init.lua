local settings = {
    number = true,
    relativenumber = true,
    cursorline = true,
    expandtab = true,
    tabstop = 4,
    shiftwidth = 4,
    colorcolumn = "80",
    termguicolors = true,
    mouse = 'a',
    undofile = true,
}

-- Generic vim.o
for k, v in pairs(settings) do
    vim.o[k] = v
end

-- Custom vim.o
vim.o.clipboard = 'unnamedplus'

-- Custom Leader
vim.g.mapleader = ' '

-- Custom Stuff that cannot be done in vim.o
vim.cmd('set undodir=$HOME/.vimdid')
vim.cmd('syntax enable')
vim.api.nvim_set_keymap('i', 'jj', '<Esc>', {})

local cmd = vim.api.nvim_command
local fn = vim.fn
local packer = nil

local function packer_verify()
    local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'

    if fn.empty(fn.glob(install_path)) > 0 then
        fn.system({ 'git', 'clone', 'https://github.com/wbthomason/packer.nvim', install_path })
        cmd 'packadd packer.nvim'
    end
end

local function packer_startup()
    if packer == nil then
        packer = require'packer'
        packer.init()
    end

    local use = packer.use
    packer.reset()

    
    -- Packer
    use 'wbthomason/packer.nvim'

    -- Telescope
    use {
        'nvim-telescope/telescope.nvim',
        requires = { {'nvim-lua/plenary.nvim'} }
    }
    vim.api.nvim_set_keymap('n', '<leader>ff', '<cmd>Telescope find_files<cr>', {noremap=true})
    vim.api.nvim_set_keymap('n', '<leader>fg', '<cmd>Telescope live_grep<cr>', {noremap=true})

    use {
        'jvgrootveld/telescope-zoxide'
    }
    vim.api.nvim_set_keymap(
        "n",
        "<leader>cd",
        ":lua require'telescope'.extensions.zoxide.list{}<CR>",
        {noremap = true, silent = true}
    )

    -- Color Schemes
    use {
        'tomasr/molokai'
    }
    vim.cmd('colorscheme molokai')

    -- Treesitter
    use {
        'nvim-treesitter/nvim-treesitter',
        run = ':TSUpdate | :TSInstall c | :TSInstall lua | :TSInstall rust'
    }


end

packer_verify()
packer_startup()
