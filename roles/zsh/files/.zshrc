# -- ZSH/oh-my-zsh stuff --
export ZSH="$HOME/.oh-my-zsh"
ZSH_THEME="jtriley"

# Automatically update without prompting.
DISABLE_UPDATE_PROMPT="true"

# Change auto update days. (Default 14)
export UPDATE_ZSH_DAYS=7

# Change the command execution time stamp shown in the history command output.
HIST_STAMPS="yyyy-mm-dd"

# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
plugins=(
  # git tools
  git
  git-auto-fetch # auto-fetches repo on directory change
  git-prompt # shows git status in prompt

  # zsh utils
  zsh-autosuggestions
  zsh-syntax-highlighting

  # aliases + completions
  docker
  docker-compose
  fd
  gh # github cli
  httpie
  helm
  kubectl
  pipenv
  ripgrep

  # misc
  colored-man-pages 
)

source $ZSH/oh-my-zsh.sh

# -- Path Stuff --
export PATH=/opt:$PATH
export PATH=$PATH:~/.anaconda3/bin
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH" # krew
export PATH="$HOME/.poetry/bin:$PATH" # poetry
export PATH="$HOME/.local/bin:$PATH" # local bin
export PATH="$HOME/.yarn/bin:$PATH" # yarn
export PATH=$PATH:$HOME/.pulumi/bin # pulumi
export PATH=$PATH:/usr/local/go/bin:$HOME/go/bin/ #go

alias rails='bundle exec rails'
alias rake='bundle exec rake'

# -- Completions, sourcing and similar --
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
eval "$(zoxide init zsh)"

# -- Aliases --
alias ll="exa -l -a -T --level=1 --git"
alias lg="lazygit"
alias j="just"
alias jr="just run"
alias je="just --edit"
alias jl="just --list"
alias vim="lvim"
alias e="vim"

alias logs="just logs"
alias up="just up"
alias down="just down"

alias p="pulumi"
alias pu="pulumi up"
alias puy="pulumi up -y"
alias pd="pulumi down"
alias pdy="pulumi down -y"

alias kc="kubectl create --dry-run=client -o yaml"

alias gdc="git diff --cached"
alias ghpr="gh pr create --fill --draft"

alias wl="bat ~/git/obsidian/worklog.md"
wla() {
  echo "$(date +"%Y-%m-%d") $@" >> ~/git/obsidian/worklog.md
}

netdbg() {
  docker run -it --net container:$1 -v ~/netbdg:/data nicolaka/netshoot
}


export EDITOR=lvim # May cause issues if lunarvim isn't setup.

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/jacob/git/tmp/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/jacob/git/tmp/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/jacob/git/tmp/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/jacob/git/tmp/google-cloud-sdk/completion.zsh.inc'; fi
